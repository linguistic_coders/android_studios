package com.example.Support_Section;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Spinner spinner=findViewById(R.id.state_spinner);
        //getting the recyclerview from xml
         recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setVisibility(View.INVISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setNestedScrollingEnabled(false);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0) {
                        recyclerView.setVisibility(View.INVISIBLE);
                        recyclerView.setAdapter(new CardAdapter(MainActivity.this,null));
                    return;
                }
                recyclerView.setVisibility(View.VISIBLE);
                switch(parent.getSelectedItem().toString()){
                    case "Karnataka":
                    { //initializing the Cardlist
                        ArrayList CardList = new ArrayList<>();
                        //adding some items to our list
                        CardList.add(
                                new Card(
                                        "Sunaad Kannada School For Hearing Impared","96, 1st cross Rd, 1st Stage, Kadugondanahalli, Bengaluru, Karnataka 560084","+917022460158",R.drawable.sunaad));
                        CardList.add(
                                new Card(
                                        "J S S Sahana Integrated & Special School","JSS 38th, 1st Main Rd, 8th Block, 7th Block, Jayanagar, Bengaluru, Karnataka 560070","+918022970127",R.drawable.sahana));
                        CardList.add(
                                new Card(
                                        "National Residential School For The Deaf","2250, 1st A Cross Road, Nanja Reddy Colony, Jeevan Bima Nagar, Nanja Reddy Colony, Jeevan Bima Nagar, Bengaluru, Karnataka 560017","+919986630182",R.drawable.nrsftd));
                        CardList.add(
                                new Card(
                                        "The Sheila Kothavala Institute For The Deaf","No:28, Rustum Bagh Main Rd, Rustam Bagh Layout, Old Airport Road, Rustam Bagh Layout, Bengaluru, Karnataka 560008","+9108025262274",R.drawable.shiela));
                        CardList.add(
                                new Card(
                                        "St. Agnes Special School","KMC Mercara Trunk Rd, Opposite St.Agnes College, Mallikatte, Kadri, Mangaluru, Karnataka 575002","+9108242443376",R.drawable.agnes));
                        CardList.add(
                                new Card(
                                        "SDM Mangala Jyothi Integrated School","Vamanjoor, Mangaluru, Karnataka 575028","+918242262030",R.drawable.mangala));
                        CardList.add(
                                new Card(
                                        "Deaf & Dumb School","Gurunath Nagar Old Hb-24, Old Hubli, Old Hubli, Mehboob Nagar Circle, Jannat Nagar, Old Hubli, Hubballi, Karnataka 580024","+918362305244",R.drawable.dnds));
                        //creating recyclerview adapter
                        CardAdapter adapter = new CardAdapter(MainActivity.this, CardList);
                        //setting adapter to recyclerview
                        recyclerView.setAdapter(adapter);
                    }
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
